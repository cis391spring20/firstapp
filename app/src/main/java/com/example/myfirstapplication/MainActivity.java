package com.example.myfirstapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    
    private ImageView leadingCard;
    private ImageView trailingCard;
    private TextView tvPlayerScore;
    private TextView tvComputerScore;
    private int playerScore = 0;
    private int computerScore = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        leadingCard = findViewById(R.id.leadingCard);
        trailingCard = findViewById(R.id.trailingCard);

        tvPlayerScore = findViewById(R.id.tvPlayerScore);
        tvComputerScore = findViewById(R.id.tvComputerScore);
    }


    public void btnDeal_Click(View view) {
        int leadingCardNum = getRandomCardNumber(56);
        int trailingCardNum = getRandomCardNumber(12);
        if (leadingCardNum != trailingCardNum) {
            if (leadingCardNum > trailingCardNum) {
                this.playerScore++;
            } else {
                this.computerScore++;
            }
        }

        leadingCard.setImageResource(getCard(leadingCardNum));
        trailingCard.setImageResource(getCard(trailingCardNum));
        tvComputerScore.setText(this.computerScore + "");
        tvPlayerScore.setText(this.playerScore + "");

    }

    private int getRandomCardNumber(int seed) {
        return new Random(System.currentTimeMillis() + seed).nextInt(14 - 2 + 1) + 2;
    }

    private int getCard(int card) {
        return getResources().getIdentifier("card" + card, "drawable", "com.example.myfirstapplication");
    }
}
